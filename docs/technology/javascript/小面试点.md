---
title: 小面试点
---

# 一些简单但你也必须懂的基础

## Object.create
作用：Object.create可以用来创建一个纯净的空对象,这个对象没有原型,如`Object.create(null)`

原理：实际上是创建一个空对象，将空对象的__proto__指向第一个参数

> [Object.create()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create)
```js
// 比如Object.create(null)的实现
const obj = {}
obj.__proto__ = null
```
---
## new原理
new的过程其实不复杂，直接看MDN上的描述
> [new 运算符](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/new)


> new 关键字会进行如下的操作：
>
> 1. 创建一个空的简单JavaScript对象（即{}）；
> 2. 为步骤1新创建的对象添加属性__proto__，将该属性链接至构造函数的原型对象 ；
> 3. 将步骤1新创建的对象作为this的上下文 ；
> 4. 如果该函数没有返回对象，则返回this。

```js
const _new = function (fn, ...args) {
    /* 第一步相当于
    const obj = {}
    obj.__proto__ = fn.prototype
    */
    const obj = Object.create(fn.prototype);
    const ret = fn.apply(obj, args);
    return ret instanceof Object ? ret : obj;
}
```

---




## import.meta

> [import.meta](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/import.meta)

import.meta对象由一个关键字"import",一个点符号和一个meta属性名组成。通常情况下"import."是作为一个属性访问的上下文，但是在这里"import"不是一个真正的对象。

import.meta对象是由ECMAScript实现的，它带有一个null的原型对象。这个对象可以扩展，并且它的属性都是可写，可配置和可枚举的。


---
## sort

> [sort](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/sort)
> 
> [深入浅出 JavaScript 的 Array.prototype.sort 排序算法](https://segmentfault.com/a/1190000010648740)

`sort()` 方法用[原地算法](https://en.wikipedia.org/wiki/In-place_algorithm)对数组的元素进行排序，并返回数组。

默认排序顺序是在将元素转换为字符串，然后比较它们的UTF-16代码单元值序列时构建的

由于它取决于具体实现，因此无法保证排序的时间和空间复杂性。

> 在计算机科学中，原地算法是一种不使用辅助数据结构转换输入的算法。但是，允许为辅助变量提供少量额外的存储空间。当算法执行时，输入通常被输出覆盖。原地算法只通过替换或者交换元素来更新它的输入序列。没有就位的算法有时被称为不就位或不就位。

如果没有指明 `compareFunction` ，那么元素会按照转换为的字符串的诸个字符的Unicode位点进行排序。例如 "Banana" 会被排列到 "cherry" 之前。当数字按由小到大排序时，9 出现在 80 之前，但因为（没有指明 `compareFunction`），比较的数字会先被转换为字符串，所以在Unicode顺序上 "80" 要比 "9" 要靠前。

ECMA规范中并没有明确定义用哪种排序方式实现sort方法

### 各个浏览器有自己的实现方式

| 浏览器                   | 使用的 JavaScript 引擎   | 排序算法           |
| ------------------------ | ------------------------ | ------------------ |
| Google Chrome            | V8                       | 插入排序和快速排序 |
| Mozilla Firefox          | SpiderMonkey             | 归并排序           |
| Safari                   | Nitro（JavaScriptCore ） | 归并排序和桶排序   |
| Microsoft Edge 和 IE(9+) | Chakra                   | 快速排序           |

`V8` 引擎的一段注释

```javascript
// In-place QuickSort algorithm.
// For short (length <= 10) arrays, insertion sort is used for efficiency.
```

`Google Chrome` 对 `sort` 做了特殊处理，对于长度 `<= 10` 的数组使用的是插入排序(稳定排序算法) ，`>10` 的数组使用的是快速排序。快速排序是不稳定的排序算法。

### 各种算法的对比

| 排序类型 | 平均情况 | 最好情况 | 最坏情况 | 辅助空间 | 稳定性 |
| -------- | -------- | -------- | -------- | -------- | ------ |
| 快速排序 | O(nlogn) | O(nlogn) | O(n²)    | O(nlogn) | 不稳定 |
| 归并排序 | O(nlogn) | O(nlogn) | O(nlogn) | O(n)     | 稳定   |
| 插入排序 | O(n²)    | O(n)     | O(n²)    | O(1)     | 稳定   |
| 桶排序   | O(n+k)   | O(n+k)   | O(n²)    | O(n+k)   | 不稳定 |


---

## indexOf
> [indexOf](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf)

`indexOf()`方法返回在数组中可以找到一个给定元素的第一个索引，如果不存在，则返回-1。
通过查看[ECMA最新规范](https://262.ecma-international.org/6.0/#sec-array.prototype.indexof)indexOf的实现，可知它是线性遍历查找字符串，时间复杂度是O(n)

---

## 柯里化

### 已知传入函数的参数个数 

```js
function create_curry(func, ...args) {
    let argity = func.length;// 获取函数参数个数
    args = args || [];
    return function () {
        args = [...args, ...arguments];
        if (args.length >= argity) {
            // 如果参数个数上限了则执行函数
            return func.apply(this, args);
        }
        return create_curry(func, ...args);
    }
}
// 使用举例
function check(reg, targetString) {
    return reg.test(targetString);
}
var _check = create_curry(check);

var checkPhone = _check(/^1[34578]\d{9}$/);
// console.log("checkPhone('183888888')", checkPhone('13415055850'))
```
### 面试题：不知道函数参数的个数

我们先稍微分析一下：上面的柯里化我们知道函数参数个数，所以我们可以知道调用函数的时机

但是这道面试题不知道参数个数，那么是无法通过参数的多少来判断函数执行的时机

我们最终只能得到一个函数，只要这个函数执行我们就能获得结果

但我们不想手动执行，那么只能考虑在某个时间点偷偷的调用函数，得到结果

这个时间点，就是JS引擎给我们打印结果的时机，当我们打印一个函数时，JS引擎会自动调用toString函数

我们只需要重写toString函数既可

> 新版Chorme中`console.log`的实现方式不太一样，没有调用toString，我们可以通过`alert(add(1)(2)(3))`来查看结果

```js
function add(...args) {
    args = args || [];
    function adder() {
        args = [...args, ...arguments];
        return adder;
    }
    // console.log的时候，会将内容转为字符串，在这个时机会调用toString函数
    adder.toString = function () {
        return args.reduce((cur, res) => res += cur);
    }
    return adder;
}

// add(1)(2)(3) = 6;
// add(1, 2, 3)(4) = 10;
// add(1)(2)(3)(4)(5) = 15;
```

---
## 防抖
### 延迟防抖（延迟时间结束触发）
```js
const debounce_delay = function (func, wait) {
    let timer = null;
    return function () {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, arguments);
        }, wait);
    }
}
```
### 立即防抖（周期时间开始时触发）
```js
const debounce_now = function (func, wait) {
    let timer = null;
    return function () {
        clearTimeout(timer);
        let call_now = !timer;
        timer = setTimeout(() => {
            timer = null;
        }, wait);
        if (call_now) func.apply(this, arguments);
    }
}
```
### 二合一版本
```js
const _debounce = function (func, wait, immediate = false) {
    let timer = null;
    return function () {
        clearTimeout(timer);
        if (immediate) {
            let call_now = !timer;
            timer = setTimeout(() => {
                timer = null;
            }, wait);
            if (call_now) func.apply(this, arguments);
        } else {
            timer = setTimeout(() => {
                func.apply(this, arguments);
            }, wait);
        }
    }
}
```


---
## 节流
### 节流定时器版本
```js
const throttle__setTimeout = function (func, time) {
    let can_run = true;
    return function () {
        if (!can_run) return;
        can_run = false;
        setTimeout(() => {
            func.apply(this, arguments);
            can_run = true;
        }, time);
    }
}
```
### 节流时间戳版本
```js
const throttle__timeStamp = function (func, time) {
    let pre = 0;
    return function () {
        let now = Date.now();
        if (now - pre < time) return;
        func.apply(this, arguments);
        pre = now;
    }
}
```
---
## 原生XMLRequest
注意：兼容性问题
1. 如果不需要通过请求头发送数据，send(null)要将null传入
2. 为了确保跨浏览器兼容性，建议将onreadystatechange事件处理程序放在open方法前面
3. setRequestHeader必须写在open和send方法之间

```js
const $ = {
  get(url, cb) {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        cb.call(this, xhr.responseText);
      }
    };
    xhr.open("get", url);
    xhr.send(null);
  },
};
```
### 怎么终止请求？

可以手动调用xhr.abort()方法取消请求

### 进度条实现？
```js
xhr.upload.onprogess = function (e) {
  e.total; //总大小
  e.loaded; //已经上传的大小
};
xhr.onprogress = function (e) {
  e.position; //表示已经接收的字节数
  e.totalSize; //表示根据Content-Length响应头确定的预期字节数
};
```

---
## 面试题：请求10张图片，并发量为3，一张请求成功后如何挑选另外一张去继续请求

> [Promise面试题详解之控制并发](https://www.jb51.net/article/212277.htm)

分析：利用Promise.race可以实现
1. Promise.race 只要有一个promise对象进入 FulFilled 或者 Rejected 状态的话，就会继续进行后面的处理
2. 首先，先挑选3张图片去作为一个promises
3. 通过Promise.race并发请求
4. 当其中一个请求到达的时候会触发回调，将到达的promise替换成新的promise，继续并发请求
```js
const urls = [
  'https://www.kkkk1000.com/images/getImgData/getImgDatadata.jpg',
  'https://www.kkkk1000.com/images/getImgData/gray.gif',
  'https://www.kkkk1000.com/images/getImgData/Particle.gif',
  'https://www.kkkk1000.com/images/getImgData/arithmetic.png',
  'https://www.kkkk1000.com/images/getImgData/arithmetic2.gif',
  'https://www.kkkk1000.com/images/getImgData/getImgDataError.jpg',
  'https://www.kkkk1000.com/images/getImgData/arithmetic.gif',
  'https://www.kkkk1000.com/images/wxQrCode2.png'
];
/**
 * 加载对应url的图片
 * @param {*} src
 */
function loadOneImg(src) {
  return new Promise((resolve, reject) => {
    try {
      const img = new Image();
      img.src = src;
      img.onload = function () {
        console.log('url:',src,'的图片加载成功')
        resolve();
      };
      img.onerror = function () {
        reject();
      };
    } catch (e) {
      reject();
    }
  });
}
/**
 * @param {*} limit 并发量
 */
function loadImgs(limit) {
  let copy = urls.slice();// 复制一份
  let promises = [];
  // 先组成一个promises数组，promises数量为limit，返回index
  // index是promises数组中每个promise对应的位置
  promises = copy.splice(0, limit).map((url, index) => loadOneImg(url).then(() => index));
  let p = Promise.race(promises)//第一次并发请求
  for (let i = 0; i < copy.length; i++) {//遍历剩余的url
    // 每次并发请求由p控制
    p = p.then(index => {
      // 上一次并发请求成功后，将数组中对应的promise替换成新的promise
      promises[index] = loadOneImg(copy[i]).then(() => index)
      // 重新并发请求
      return Promise.race(promises)
    })
  }
}
// 放到浏览器控制台执行
loadImgs(3);
```
---
 ## 如何取消一个promise?

分析：借助Promise.race的特性
1. 我们自定义一个mypromise,将mypromise,promise两个一起用race发送
2. 这样我们调用mypromise的resolve或者reject函数的时候就能够在promise请求成功前拦截

 ```js
function wrapper(promise) {
  const wrap = {};// 存储自定义promise的resolve和reject事件，以便随时调用
  const p = new Promise((resolve, reject) => {
    wrap.resolve = resolve;
    wrap.reject = reject;
  });
  wrap.promise = Promise.race([p, promise]);//存储并发请求
  return wrap;
}
// 测试
const testp = new Promise((resolve) => {
    setTimeout(() => {
      resolve("success");
    }, 2000);
  });
const p = wrapper(testp);
p.promise.then((res)=>{
    console.log(res);
})
p.resolve('stop http');// 改变自定义promise的状态，阻止另外一个请求
 ```
---
## 图片转base64

```js
const imgURL = "./01.png"; //图片链接
// 图片转base64
function toBase64(image) {
  var canvas = document.createElement("canvas");
  canvas.width = image.width;
  canvas.height = image.height;
  const ctx = canvas.getContext("2d");
  ctx.drawImage(image, 0, 0, image.width, image.height);//将图片画到画布上
  const ext = image.src.substring(image.src.lastIndexOf(".") + 1).toLowerCase();//图片后缀png
  const dataURL = canvas.toDataURL("image/" + ext);//将画布转为指定的base64格式，这里是image/png
  return dataURL;
}
const image = new Image();
image.src = imgURL;
image.onload = function () {
  const base64 = toBase64(image);
  console.log(base64);
};
```
---
## 上传前预览图片
```html
<input type="file" name="" id="fileInput">
<script>
    // 上传图片前预览
    const oInput = document.getElementById("fileInput");
    oInput.addEventListener('change', function (e) {
        console.log(e.target.files);
        const file = e.target.files[0];//file 对象
        if(file){
            // base64读取文件对象
            const reader = new FileReader();
            reader.readAsDataURL(file);//将文件读取为dataURL(base64)
            reader.onload = function(e){
                console.log(e.target.result);//获取到base64格式字符串
                // 将该字符串放到对应的img标签即可预览
            }
        }
    })
</script>
```

---
## 模块化
### 方案一：立即执行函数(IFFE)
```js
(function (window) {
    window.test = function () {

    }
})(window)
```
### 方案二：AMD

```js
define(['./a', './b'], function (a, b) {
    // 加载完毕的回调
    a.do();
    b.do();
})
```
### 方案三：CMD
```js 
define(function (require, exports, module) {
    // 延迟加载
    var a = require('./a');
    a.do();
})
```
### 方案四：CommonJS
```js
// module是node特有的一个对象
module.exports = {
    a: 1
}
// 基本实现
module = {
    id:'',
    exports: {},
}
var exports = module.exports;
var load = function(module){
    // 导出的东西
    var a = 1;
    module.exports = a;
    return module.exports;
}
// 然后当执行require的时候去找到对应id的module
// 将要使用的东西用IFFE包装一下，over
```
---
## localStorage安全问题
localStorage存在安全问题，封装溢出逻辑的localStorage，主要是要知道溢出抛出的错误事件名称(QuotaExceededError)
```js
localStorage.set = (key, value) => {
  try {
    localStorage.setItem(key, value);
  } catch (err) {
    console.log(err);
    if (err.name === "QuotaExceededError") {
      console.log("已超出5MB的存储大小！");
      localStorage.clear();
      localStorage.setItem(key, value);
    }
  }
};
```
封装过期时间的localStorage
```js
class LocalStorage {
  /**
   * @param {Number} exp 过期时间
   */
  constructor(exp) {
    this.exp = exp;
    this._items = {};
  }
  setItem(key, val) {
    this._items[key] = {
      value: val,
      time: Date.now(),
    };
  }
  getItem(key) {
    const item = this._items[key];
    if (Date.now() - item.time > this.exp) {
      console.log("expires");
    } else {
      return item.value;
    }
  }
}
```
---
## 非匿名自执行函数的函数名只读
作用域：执行上下文包含作用域链

作用域，可以理解为执行上下文中声明的变量和作用范围；包括块级作用域和函数作用域

特性：声明提升，函数声明优先于变量声明

在非匿名自执行函数中，函数名是只读状态，无法修改
```js
var b = 10;
(function b() {
  b = 20;
  console.log(b);//function b(){}
})();
console.log(b)//10;
```
---
## 手写Event类

```js
function Event() {
  this._events = {};
}
// on事件监听
Event.prototype.on = function (type, fn) {
  if (!this._events[type]) {
    this._events[type] = [];
  }
  this._events[type].push(fn);
};
// off移除监听
Event.prototype.off = function (type, fn) {
  if (!this._events[type]) {
    return;
  }
  if (!fn) {
    // 无指定函数，全部移除
    this._events[type] = undefined;
    return;
  }
  const index = this._events[type].indexOf(fn);
  this._events[type].splice(index, 1);
};
// emit触发事件
Event.prototype.emit = function (type) {
  if (!this._events[type]) {
    return;
  }
  this._events[type].forEach((f) => f());
};
// once只触发一次
Event.prototype.once = function (type, fn) {
  const _ = this;
  const _fn = function () {
    _.off(type, _fn);
    fn.apply(_, arguments);
  };
  this.on(type, _fn);
};
```

