---
home: true
heroImage: /images/logo.jpg
heroText: 钱端
tagline: 程序员休闲好去处
actionText: 学习技术 →
actionLink: /technology/
features:
- title: 生活
  details: 分享工作，面试，日常琐事等经历
- title: 技术
  details: 分享技术、踩坑日记，用普通话讲解技术
footer: MIT Licensed | Copyright © 2021-8-21 wzp-coding
---
<!-- 
# 欢迎来到本站

## 自我介绍

大家好，我来自蓝色星球中国某某省某某市某某区某某镇某某村，很高兴认识你

## 站点特色

本站记录了我从大三开学至今的博客，技术，生活等等，今天去看医生了，医生说我只能活到100岁，唉，那这个站点可能只能更新到2101年了 -->

